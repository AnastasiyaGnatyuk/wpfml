﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfML
{
    /// <summary>
    /// Логика взаимодействия для PageSour.xaml
    /// </summary>
    public partial class PageSour : Page
    {
        public PageSour()
        {
            this.ShowsNavigationUI = false;
            InitializeComponent();
        }

        private void _Click_2(object sender, RoutedEventArgs e)
        {
            MainPage task = new MainPage();
            task.ShowsNavigationUI = false;
            this.NavigationService.Navigate(task);
        }

        private void _Click_1(object sender, RoutedEventArgs e)
        {
            DowloadPage task = new DowloadPage();
            this.NavigationService.Navigate(task);
        }
    }

}
