﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace WpfML
{
    /// <summary>
    /// Логика взаимодействия для TaskPage.xaml
    /// </summary>
    public partial class DowloadPage : Page
    {
        public DowloadPage()
        {
            InitializeComponent();
        }

        private void Folder_drop(object sender, System.Windows.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                string[] filePaths = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                try
                {
                    string[] name_path = filePaths[0].Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                    var name_folder = name_path[name_path.Length - 1];
                    n_folder.Text = name_folder;

                    var vse_f = Directory.GetFiles(filePaths[0]);

                    int t = 0;
                    var path_vse = new List<string>();

                    foreach (var s in vse_f)
                    {
                        var flag = s.Contains("tags.tsv");
                        if (flag == false)
                        {
                            string[] s_ = s.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                            var s_n = s_[s_.Length - 1];
                            path_vse.Add(s_n);
                        }
                    }

                    var mL = new ML();
                    mL.Train(filePaths[0],path_vse);

                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }

        private void Back_now(object sender, RoutedEventArgs e)
        {
            PageSour task = new PageSour();
            this.NavigationService.Navigate(task);
        }

        //private void image_drop_1(object sender, System.Windows.DragEventArgs e)
        //{
        //    if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
        //    {
        //        string[] filePaths = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
        //        try
        //        {
        //            BitmapImage sourceViewBitmap = new BitmapImage();
        //            sourceViewBitmap.BeginInit();
        //            sourceViewBitmap.UriSource = new Uri(filePaths[0], UriKind.RelativeOrAbsolute);
        //            sourceViewBitmap.CacheOption = BitmapCacheOption.OnLoad;
        //            sourceViewBitmap.EndInit();
        //            imageOriginal_1.Source = sourceViewBitmap;
        //        }
        //        catch (Exception ex)
        //        {
        //            System.Windows.MessageBox.Show(ex.Message);
        //        }
        //    }
        //}
    }
}
