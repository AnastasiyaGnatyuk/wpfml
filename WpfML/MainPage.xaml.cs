﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfML
{
    /// <summary>
    /// Логика взаимодействия для MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //private void EscButton_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Close();
        //}

        private void Image_drop(object sender, System.Windows.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                string[] filePaths = (string[])e.Data.GetData(System.Windows.DataFormats.FileDrop);
                try
                {
                    BitmapImage sourceViewBitmap = new BitmapImage();
                    sourceViewBitmap.BeginInit();
                    sourceViewBitmap.UriSource = new Uri(filePaths[0], UriKind.RelativeOrAbsolute);
                    sourceViewBitmap.CacheOption = BitmapCacheOption.OnLoad;
                    sourceViewBitmap.EndInit();
                    var n = sourceViewBitmap.UriSource.Segments.Length;
                    var name = sourceViewBitmap.UriSource.Segments[n-1];
                    imgPicture.Source = sourceViewBitmap;
                    var namepath = sourceViewBitmap.UriSource.LocalPath;

                    var str = "\\" + name;
                    namepath = namepath.Replace(str, "");

                    ML.OutPredict outPredict = ML.Main(namepath, name);
                    double outpr = outPredict.Score * 100;

                    str = outPredict.PredictValue + " as: " + Math.Round(outpr) + "%";
                    var num = Math.Round(outpr);
                    if (num >= 80)
                        textBox.Foreground = Brushes.Lavender;
                    else
                    {
                        if ((num < 80) && (num >= 50))
                            textBox.Foreground = Brushes.Purple;
                        else textBox.Foreground = Brushes.Red;
                    }
                    //textBox.Foreground=Brushes.Lavender;
                    textBox.Text = str;
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }     
        }

        private void Back_now(object sender, RoutedEventArgs e)
        {
            PageSour task = new PageSour();
            this.NavigationService.Navigate(task);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofdPicture = new OpenFileDialog();
            ofdPicture.Filter =
                "Image files|*.bmp;*.jpg;*.gif;*.png;*.tif|All files|*.*";
            ofdPicture.FilterIndex = 1;

            if (ofdPicture.ShowDialog() == true)
                imgPicture.Source =
                    new BitmapImage(new Uri(ofdPicture.FileName));


            var namepath = ofdPicture.FileName;
            var name = ofdPicture.SafeFileName;

            var str = "\\" + name;
            namepath = namepath.Replace(str, "");

            ML.OutPredict outPredict = ML.Main(namepath, name);
            double outpr = outPredict.Score * 100;

            str = outPredict.PredictValue + " as: " + Math.Round(outpr) + "%";
            var num = Math.Round(outpr);
            if (num >= 80)
                textBox.Foreground = Brushes.Lavender;
            else
            {
                if ((num < 80) && (num >= 50))
                    textBox.Foreground = Brushes.Purple;
                else textBox.Foreground = Brushes.Red;
            }
            //textBox.Foreground=Brushes.Lavender;
            textBox.Text = str;
        }

        private void Button1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
