﻿using Microsoft.ML;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Data;
using Microsoft.ML.ImageAnalytics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferLearningTF;

namespace WpfML
{
    class ML
    {
        // <SnippetDeclareGlobalVariables>
        static readonly string _assetsPath = Path.Combine(Environment.CurrentDirectory, "assets");
        static readonly string _trainTagsTsv = Path.Combine(_assetsPath, "inputs-train", "data", "tags.tsv");
        static readonly string _predictImageListTsv = Path.Combine(_assetsPath, "inputs-predict", "data", "image_list.tsv");
        static readonly string _trainImagesFolder = Path.Combine(_assetsPath, "inputs-train", "data");
        //static readonly string _predictImagesFolder = Path.Combine(_assetsPath, "inputs-predict", "data");
        static readonly string _predictSingleImage = Path.Combine(_assetsPath, "inputs-predict-single", "data", "toaster3.jpg");
        static readonly string _inceptionPb = Path.Combine(_assetsPath, "inputs-train", "inception", "tensorflow_inception_graph.pb");
        static readonly string _inputImageClassifierZip = Path.Combine(_assetsPath, "inputs-predict", "imageClassifier.zip");
        static readonly string _outputImageClassifierZip = Path.Combine(_assetsPath, "outputs", "imageClassifier.zip");
        private static string LabelTokey = nameof(LabelTokey);
        private static string ImageReal = nameof(ImageReal);
        private static string PredictedLabelValue = nameof(PredictedLabelValue);
        // </SnippetDeclareGlobalVariables>

        public static OutPredict Main(string _predictImagesFolder,string name)
        {

            using (StreamWriter sw = new StreamWriter(_predictImageListTsv, false, System.Text.Encoding.Default))
            {
                sw.Write(name);
                sw.Write("\t");
                name = name.Substring(0, name.Length - 4);
                sw.Write(name);
            }

           
            MLContext mlContext = new MLContext(seed: 1);
            
            //ReuseAndTuneInceptionModel(mlContext, _trainTagsTsv, _trainImagesFolder, _inceptionPb, _outputImageClassifierZip);
         
            OutPredict outPredict = ClassifyImages(mlContext, _predictImageListTsv, _predictImagesFolder, _outputImageClassifierZip);
            
            //ClassifySingleImage(mlContext, _predictSingleImage, _outputImageClassifierZip);

            return outPredict;
        }

        public void Train(string _trainImagesFolder, List<string> names)
        {
            foreach (var name in names)
            {
                using (StreamWriter sw = new StreamWriter(_trainTagsTsv, false, System.Text.Encoding.Default))
                {
                    sw.Write(name);
                    sw.Write("\t");
                    var str = name.Substring(0, name.Length - 4);
                    sw.Write(str);
                }
            }

            MLContext mlContext = new MLContext(seed: 1);

            ReuseAndTuneInceptionModel(mlContext, _trainTagsTsv, _trainImagesFolder, _inceptionPb, _outputImageClassifierZip);

        }



        public struct OutPredict
        {
            public string PredictValue;
            public float Score;
        }

        private struct InceptionSettings
        {
            public const int ImageHeight = 224;
            public const int ImageWidth = 224;
            public const float Mean = 117;
            public const float Scale = 1;
            public const bool ChannelsLast = true;
        }

        // Build and train model
        public static void ReuseAndTuneInceptionModel(MLContext mlContext, string dataLocation, string imagesFolder, string inputModelLocation, string outputModelLocation)
        {
            var data = mlContext.Data.ReadFromTextFile<ImageData>(path: dataLocation, hasHeader: false);

            var estimator = mlContext.Transforms.Conversion.MapValueToKey(outputColumnName: LabelTokey, inputColumnName: DefaultColumnNames.Label)
                            // The image transforms transform the images into the model's expected format.
                            .Append(mlContext.Transforms.LoadImages(_trainImagesFolder, (ImageReal, nameof(ImageData.ImagePath))))
                            .Append(mlContext.Transforms.Resize(outputColumnName: ImageReal, imageWidth: InceptionSettings.ImageWidth, imageHeight: InceptionSettings.ImageHeight, inputColumnName: ImageReal))
                            .Append(mlContext.Transforms.ExtractPixels(new ImagePixelExtractorTransformer.ColumnInfo(name: "input", inputColumnName: ImageReal, interleave: InceptionSettings.ChannelsLast, offset: InceptionSettings.Mean)))
                            
                            // The ScoreTensorFlowModel transform scores the TensorFlow model and allows communication 
                            
                            .Append(mlContext.Transforms.ScoreTensorFlowModel(modelLocation: inputModelLocation, outputColumnNames: new[] { "softmax2_pre_activation" }, inputColumnNames: new[] { "input" }))
                            
                            .Append(mlContext.MulticlassClassification.Trainers.LogisticRegression(labelColumn: LabelTokey, featureColumn: "softmax2_pre_activation"))
                            
                            .Append(mlContext.Transforms.Conversion.MapKeyToValue((PredictedLabelValue, DefaultColumnNames.PredictedLabel)));

            // Train the model
            Console.WriteLine("=============== Training classification model ===============");
            // Create and train the model based on the dataset that has been loaded, transformed.

            ITransformer model = estimator.Fit(data);


            // Process the training data through the model
            // This is an optional step, but it's useful for debugging issues

            var predictions = model.Transform(data);

            // Create enumerables for both the ImageData and ImagePrediction DataViews 
            // for displaying results
            var imageData = mlContext.CreateEnumerable<ImageData>(data, false, true);
            var imagePredictionData = mlContext.CreateEnumerable<ImagePrediction>(predictions, false, true);

            // Read the tags.tsv file and add the filepath to the image file name 
            // before loading into ImageData
            PairAndDisplayResults(imageData, imagePredictionData);

            // Get some performance metrics on the model using training data
            Console.WriteLine("=============== Classification metrics ===============");

            // <SnippetEvaluate>           
            var regressionContext = new MulticlassClassificationCatalog(mlContext);
            var metrics = regressionContext.Evaluate(predictions, label: LabelTokey, predictedLabel: DefaultColumnNames.PredictedLabel);
            // </SnippetEvaluate>

            //<SnippetDisplayMetrics>
            Console.WriteLine($"LogLoss is: {metrics.LogLoss}");
            Console.WriteLine($"PerClassLogLoss is: {String.Join(" , ", metrics.PerClassLogLoss.Select(c => c.ToString()))}");
            //</SnippetDisplayMetrics>

            // Save the model to assets/outputs
            Console.WriteLine("=============== Save model to local file ===============");

            // <SnippetSaveModel>
            using (var fileStream = new FileStream(outputModelLocation, FileMode.Create))
                mlContext.Model.Save(model, fileStream);
            // </SnippetSaveModel>

            Console.WriteLine($"Model saved: {outputModelLocation}");
        }

        public static OutPredict ClassifyImages(MLContext mlContext, string dataLocation, string imagesFolder, string outputModelLocation)
        {
            Console.WriteLine($"=============== Loading model ===============");
            Console.WriteLine($"Model loaded: {outputModelLocation}");
            // Load the model
            // <SnippetLoadModel>
            ITransformer loadedModel;
            using (var fileStream = new FileStream(outputModelLocation, FileMode.Open))
                loadedModel = mlContext.Model.Load(fileStream);
            // </SnippetLoadModel>

            // Read the image_list.tsv file and add the filepath to the image file name 
            // before loading into ImageData 
            // <SnippetReadFromTSV> 
            var imageData = ReadFromTsv(dataLocation, imagesFolder);
            var imageDataView = mlContext.Data.ReadFromEnumerable<ImageData>(imageData);
            // </SnippetReadFromTSV>  

            // <SnippetPredict>  
            var predictions = loadedModel.Transform(imageDataView);
            var imagePredictionData = mlContext.CreateEnumerable<ImagePrediction>(predictions, false, true);
            // </SnippetPredict> 

            Console.WriteLine("=============== Making classifications ===============");
            // <SnippetCallPairAndDisplayResults2>
            PairAndDisplayResults(imageData, imagePredictionData);
            OutPredict outPredict = GetOutPredict(imageData, imagePredictionData);
            // </SnippetCallPairAndDisplayResults2> 
            return outPredict;
        }

        public static void ClassifySingleImage(MLContext mlContext, string imagePath, string outputModelLocation)
        {
            Console.WriteLine($"=============== Loading model ===============");
            Console.WriteLine($"Model loaded: {outputModelLocation}");
            // Load the model
            // <SnippetLoadModel2>
            ITransformer loadedModel;
            using (var fileStream = new FileStream(outputModelLocation, FileMode.Open))
                loadedModel = mlContext.Model.Load(fileStream);
            // </SnippetLoadModel2>

            // load the fully qualified image file name into ImageData 
            // <SnippetLoadImageData> 
            var imageData = new ImageData()
            {
                ImagePath = imagePath
            };
            // </SnippetLoadImageData>  

            // <SnippetPredictSingle>  
            // Make prediction function (input = ImageNetData, output = ImageNetPrediction)
            var predictor = loadedModel.CreatePredictionEngine<ImageData, ImagePrediction>(mlContext);
            var prediction = predictor.Predict(imageData);
            // </SnippetPredictSingle> 

            Console.WriteLine("=============== Making single image classification ===============");
            // <SnippetDisplayPrediction>
            Console.WriteLine($"Image: {Path.GetFileName(imageData.ImagePath)} predicted as: {prediction.PredictedLabelValue} with score: {prediction.Score.Max()} ");
            // </SnippetDisplayPrediction> 

        }

        private static OutPredict GetOutPredict(IEnumerable<ImageData> imageNetData, IEnumerable<ImagePrediction> imageNetPredictionData)
        {
            IEnumerable<(ImageData image, ImagePrediction prediction)> imagesAndPredictions = imageNetData.Zip(imageNetPredictionData, (image, prediction) => (image, prediction));

            OutPredict outPredict;
            outPredict.PredictValue = "";
            outPredict.Score = 0;

            foreach ((ImageData image, ImagePrediction prediction) item in imagesAndPredictions)
            {
                outPredict.PredictValue = item.prediction.PredictedLabelValue;
                outPredict.Score = item.prediction.Score.Max();
            }

            return outPredict;
        }

        private static void PairAndDisplayResults(IEnumerable<ImageData> imageNetData, IEnumerable<ImagePrediction> imageNetPredictionData)
        {
            // Builds pairs of (image, prediction) to sync up for display
            // <SnippetBuildImagePredictionPairs>
            IEnumerable<(ImageData image, ImagePrediction prediction)> imagesAndPredictions = imageNetData.Zip(imageNetPredictionData, (image, prediction) => (image, prediction));
            // </SnippetBuildImagePredictionPairs>

            // <SnippetDisplayPredictions>
            foreach ((ImageData image, ImagePrediction prediction) item in imagesAndPredictions)
            {
                Console.WriteLine($"Image: {Path.GetFileName(item.image.ImagePath)} predicted as: {item.prediction.PredictedLabelValue} with score: {item.prediction.Score.Max()} ");
            }
            // </SnippetDisplayPredictions>
        }

        public static IEnumerable<ImageData> ReadFromTsv(string file, string folder)
        {
            //Need to parse through the tags.tsv file to combine the file path to the 
            // image name for the ImagePath property so that the image file can be found.

            // <SnippetReadFromTsv>
            return File.ReadAllLines(file)
             .Select(line => line.Split('\t'))
             .Select(line => new ImageData()
             {
                 ImagePath = Path.Combine(folder, line[0])
             });
            // </SnippetReadFromTsv>
        }
    }

}
